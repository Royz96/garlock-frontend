import React, { useState } from 'react';
import { ReactComponent as SvgCheckboxIcon } from '../../../assets/Icons/checkmark.svg';
import './index.scss';
type TFormField = 'text' | 'checkbox';

export interface IRequiredInput {
    required: boolean,
    requiredMsg: string
};

interface Props {
    type: TFormField,
    name: string,
    label: string,
    required?: IRequiredInput,
    error?: boolean | false,
    inputElAttributes?: React.InputHTMLAttributes<HTMLInputElement>,
    value: string | number,
    forceChangeValue?: (name: string, val: string) => void
}

const FormField: React.FC<Props> = (props) => {
    let element = null;
    const [checked, setChecked] = useState<boolean>(false);

    if (props.type === 'text') {
        element = (
            <div className="FormField FormFieldInputText">
                <div className="FormFieldWrapper">
                    <input type="text"
                        className={"FormFieldElement" +
                            (props.value.toString().length ? " notEmpty" : "") +
                            (props.error ? " isInvalid" : "")}
                        {...props.inputElAttributes}
                        required={props.required && props.required.required}
                        value={props.value}
                        name={props.name}
                    />
                    <label className="FormFieldLabel">
                        <span className="FormFieldLabelText">{props.label}</span>
                    </label>
                    <span className="FormFieldBackground"></span>
                </div>
                {props.required && props.required.required && (
                    <div className={"FormFieldValidation " + (!props.error && "isHidden")} >
                        <div className="FormFieldMessageContainer">
                            <div className="FormFieldMessage FormFieldMessageError">
                                {props.required.requiredMsg}
                            </div>
                        </div>
                    </div>
                )}
            </div>
        )
    } else if (props.type === 'checkbox') {
        element = (
            <div className="FormField FormFieldInputCheckbox">
                <div className="FormFieldWrapper" onClick={() => {props.forceChangeValue && props.forceChangeValue(props.name, checked ? "" : "1" ); setChecked(!checked)}}>
                    <input type="checkbox"
                        className={"FormFieldElement" +
                            (props.value.toString().length ? " notEmpty" : "")}
                        {...props.inputElAttributes}
                        required={props.required && props.required.required}
                        checked={checked}
                        value={checked ? "1" : "2"}
                        name={props.name}
                    />
                    <label className="FormFieldLabel">
                        <span className="FormFieldIcon">
                            <SvgCheckboxIcon className='Icon' />
                        </span>
                        <span className={"FormFieldLabelText"  + (props.error ? " isInvalid" : "") }>{props.label}</span>
                    </label>
                </div>
            </div>
        )
    }

    return element;

}

export default FormField;