import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { array } from 'prop-types';
import {typesong} from '../../Pages/Users'
import './Table.scss';
import UserDetail from '../../Pages/Details/UserDetail';
import { ComponentType } from '../../Pages/Dashboard';
export interface TD {
    valor: string|string[]
}

export interface Row {
    tds: TD[]
}
export interface tableprops{
    onrowclick:(state:Row)=>void,
    headers: string[],
    rows: Row[],
    type: ComponentType
}
const Table:React.FC<tableprops>=(props)=> {
    return (
        <div className="main-container">
            <table>
                <thead>
                    <tr>
                    {props.headers.map((header, k) => {
                        return <th key={k}>{header}</th>
                    })}
                    </tr>
                </thead>
                <tbody>
                    {props.rows.map((row, k) => {
                        return (
                        <tr key={k} onClick={()=>props.onrowclick(row)}>
                            {
                                row.tds.map((td, k)=>{
                                    return <td key={k}>{
                                        Array.isArray( td.valor ) ? 
                                        <ul>
                                            {td.valor.map( valor => <li>{valor}</li>)}
                                        </ul>
                                        : 
                                            td.valor
                                    } 
                                    </td>
                                })
                            }
                        </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default Table;