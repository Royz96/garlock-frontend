import React from 'react';
import './Footer.scss';

const Footer:React.FC<{}>=(props)=> {
    return (
        <footer className="footer">
            <h1>contact</h1>
            <div className="supergraphic_footer"></div>
        </footer>
    );
}

export default Footer;