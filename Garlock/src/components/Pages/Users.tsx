import "./styles.scss";
import "../../assets/scss/index.scss";
import React, { useState, useEffect } from "react";
import Table, { Row } from "../Atoms/Table/Table";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import RegisterPage from "./RegisterPage";
import UserDetail from "./Details/UserDetail";
import gql from 'graphql-tag';
import { useQuery } from "@apollo/react-hooks";
import "./loader.scss";
export interface typesong {
  title: string;
  id: string;
}
type evol = {
  name: string;
};

export interface pokemontype {
  id: string;
  name: string;
  number: string;
  types: string[];
  evolutions: evol[];
}
const query = gql`{
    pokemons(first:151){
      id
      name
      number
      types
      evolutions{
          name
      }
    }
  }
`;

const Users: React.FC<{}> = () => {
    const {loading, data, error}=useQuery(query);
  const [detail, setdetail] = useState<boolean>();
  const [array, setarray] = useState<any>([]);
  const [user, setuser] = useState<Row>({
    tds: []
  });
  const [register, setregister] = useState<boolean>(false);
  const rows: any = [];

//   useEffect(() => {
//     const fetchusers = async () => {
//       const axios = require("axios");
//       var arr: any = [];
//       await axios
//         .post("https://graphql-pokemon.now.sh/", {
//           query
//         })
//         .then((res: any) => {
//           console.log(res);
//           res.data.data.pokemons.map((pkmn: pokemontype) => {
//             arr = [...arr, pkmn];
//             const row: Row = {
//               tds: [
//                 {
//                   valor: pkmn.id
//                 },
//                 {
//                   valor: pkmn.name
//                 },
//                 {
//                   valor: pkmn.number
//                 },
//                 {
//                   valor: pkmn.types
//                 },
//                 {
//                   valor: pkmn.evolutions?.map(evol => evol.name) ?? ""
//                 }
//               ]
//             };
//             rows.push(row);
//           });
          
//         });
//     };
//     const a = {
//       headers: ["id", "name", "number", "type", "evolutions"],
//       rows: rows
//     };
//     setarray(a);
//     fetchusers();
//   }, []);
  const showregister = () => {
    setregister(true);
  };
  //load table content

  const renderdetails = (user: Row) => {
    setuser(user);
    setdetail(true);
  };
  const headers = ["Id", "Name", "number", "type", "evolutions"];
  return (
    <div className="Users">
      {loading ? (
        <div className="load">
          <div className="dot"></div>
          <div className="outline">
            <span></span>
          </div>
        </div>
      ) : (
        <div className="table-display">
          {register === true ? (
            <RegisterPage />
          ) : detail === true ? (
            <UserDetail headers={headers} row={user} />
          ) : (
            <div>
              <div className="btnspan">
                <div className="add-button">
                  <button onClick={showregister}>
                    <FontAwesomeIcon icon={faPlus} />
                  </button>
                </div>
              </div>
              <Table
                headers={array.headers}
                rows={array.rows}
                type="users"
                onrowclick={renderdetails}
              />
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default Users;
