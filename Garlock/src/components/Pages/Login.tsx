import './styles.scss';
import '../../assets/scss/index.scss';
import { Redirect, Link } from 'react-router-dom';
import React, { Component, useState, ContextType } from 'react';
import { IRegisterFormValues, IRegisterFormValidation } from './RegisterPage'
import FormField from '../Atoms/FormField';
import useForm, { TFieldValidation } from '../../Util/Hooks/useForm';
import Button from '../Atoms/Button';
import { Consumer } from '../../Util/Context/Context';
import { pokemontype } from './Users';
import {contexttype} from '../../Util/Context/Context';

interface serverresponse {
    data: any;
}

export interface LoginValues {
    email: string;
    password: string;
}
export interface LoginValidation {
    email: TFieldValidation;
    password: TFieldValidation;
}

interface song {
    title: string;
}
const query = `query getpkmn($name: String) {
    pokemon(name: $name) {
      id
      number
      name
      types
      evolutions {
        name
      }
    }
  }` ;

const mutation = `
  mutation AddSong($title: String){
    addSong(title: $title){
        title
    }
}
  `;

const Login: React.FC<{}> = () => {
    const [Loggeduser, setloggeduser] = useState<pokemontype>();
    

    const defaultFormValues: LoginValues = {
        email: '',
        password: '',
    }

    const formValidation: LoginValidation = {
        email: 'email',
        password: 'not-empty',
    }

    const { handleChange, values, handleBlur, errors, handleSubmit, forceUpdateValue } = useForm<LoginValues, LoginValidation>(defaultFormValues, formValidation);


    const submitForm = async (context:contexttype) => {
        const axios = require("axios");
        const response = await axios.post('https://graphql-pokemon.now.sh/', {
            query: query,
            variables: { name: values.password }
        }).then((res: any) => { return (res.data.data.pokemon === null ? false : res) });
        response===false?alert("No existe el usuario"):(context.setlogged(true))
        alert(context.logged)
    }
    return (
        <Consumer>
            {
                context => (
                    <div className="login">
                        <form>
                            <div className="logo-login">
                            </div>
                            <div className="login-container">
                                <div className="mainContainer">
                                    <div className="welcome">
                                        <h2>Bienvenido</h2>
                                        <h6>Para comenzar inicia sesión</h6>
                                    </div>
                                    <FormField
                                        type='text'
                                        label='Correo'
                                        required={{ required: true, requiredMsg: 'Por favor ingresa un correo válido.' }}
                                        name='email'
                                        value={values.email}
                                        inputElAttributes={{
                                            onChange: handleChange,
                                            onBlur: handleBlur
                                        }}
                                        error={errors.email}
                                    />
                                    <FormField
                                        type='text'
                                        label='Contraseña'
                                        required={{ required: true, requiredMsg: 'Por favor ingresa una contraseña' }}
                                        name='password'
                                        value={values.password}
                                        inputElAttributes={{
                                            onChange: handleChange,
                                            onBlur: handleBlur,
                                            type: "password"
                                        }}
                                        error={errors.password}
                                    />
                                    <div className="buttonspan">
                                        <Button type="primary" clickEvent={()=>submitForm(context)} elementAttr={{
                                            type: 'button'
                                        }} size="large">{"Ingresar"}</Button>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                )
            }
        </Consumer>

    );
}

export default Login;