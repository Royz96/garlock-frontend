import './styles.scss';
import '../../assets/scss/index.scss';
import React, { Component,useState,useEffect, useReducer } from 'react';
import Table, { Row } from '../Atoms/Table/Table';
import { Link, Redirect } from 'react-router-dom';
export interface typesong{
    title:string;
    id:string;
}
const query =`{
    songs{
        title
        id
    }
}` ;

const Clients:React.FC<{}>=()=>{
        const [array,setarray]=useState<any>([]);
        const [loading,setloading]=useState<boolean>(true);
        const rows:any = []; 
        useEffect(()=>{
            const fetch=async()=>{
            const axios=require("axios");
            var arr:any=[];
            await axios.post('http://localhost:4000/graphql',{
               query
           }).then((res:any)=>{
               res.data.data.songs.map((song:typesong)=>{
                   arr=[...arr,song];
                   const row: Row = {
                    tds: [
                     {
                         valor: song.title
                     },
                     {
                         valor: song.id
                     },
                    ] 
                };
                rows.push(row);
               });
               setloading(false)
           }
           );
            }
            const a = {
                headers: [
                    'Title',
                    'ID'
                ],
                rows: rows
            }
            setarray(a);
            fetch(); 
        },[]);
        
        return (
            <div className="Users">
                {loading? <h2>Cargando, por favor espere</h2>:
                    <div className="table-display">
                        {/* <Table headers={array.headers} rows={array.rows}/> */}
                    <h1>Clientes...</h1>
                    </div>
                }
            </div>
        );
    }

export default Clients;