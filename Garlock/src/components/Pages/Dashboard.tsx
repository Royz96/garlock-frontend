import React, { Component, useState } from 'react';
import BackDrop from '../BackDrop/BackDrop';
import Toolbar from '../Toolbar/Toolbar';
import SideDrawer from '../SideDrawer/SideDrawer'
import './styles.scss';
import Footer from '../Atoms/Footer/Footer';
import Users from './Users';
import Default from './Default';
import Clients from './Clients';

export type ComponentType = "users"|"clients"|"plants"|null;

interface DashboardState{
  sideDrawerOpen:boolean;
  comp: ComponentType;
}

class Dashboard extends Component<{},DashboardState> {
  constructor(props:any){
    super(props);
    this.state={
      sideDrawerOpen:false,
      comp:null
    }
  }
      drawerToggleClick=()=>{
        this.setState((prevState)=>{
          return {sideDrawerOpen:!prevState.sideDrawerOpen}
        });
      }
      backDropClick=()=>{
        this.setState({sideDrawerOpen:false});
      }
      switchrender=()=>{
          switch (this.state.comp) {
            case "users":
              return(<Users/>);
              break;
            case "clients":
              return(<Clients/>);
            default:
              return (<Default/>);
              break;
          }
      }

      renderComponentType = (state: ComponentType) => {
        this.setState({
          comp: state
        })
      }

      componentDidUpdate() {
        console.log('hey' ,this.state.comp)
      }
    render() {
        let backdrop;
        const load=true;
        if (this.state.sideDrawerOpen){
          backdrop=<BackDrop click={this.backDropClick}/>;
        }
          return (
            <div className="App" style={{height:'90%'}}>
            <Toolbar drawerclickhandler={this.drawerToggleClick} name="Rodrigo Silva Gonzalez" position="HCM,C/IDI-MX"/>
            <SideDrawer show={this.state.sideDrawerOpen} onNavChange={this.renderComponentType} />
            {backdrop}
            <main style={{marginTop:'97px'}}> 
                <div className="content">
                    {this.switchrender()}
                </div>
          </main>
          <Footer/>
            </div>
          );
        }
}

export default Dashboard;