import React from 'react';
import {pokemontype} from '../Users'
import { Row } from '../../Atoms/Table/Table';

interface usrdtl{
    headers:string[],
    row:Row
}
const UserDetail:React.FC<usrdtl>=(props)=>{
    console.log(props)
    return (
        <div className="detailcontainer">
            <table>
            <thead>
                <tr>
                    {props.headers.map(th=>{
                        return(<th>{th}</th>)
                    })}
                </tr>
            </thead>
            <tr>
                {props.row.tds.map(td=>{
                    return(<td>{td.valor}</td>)
                })}
            </tr>
        </table>
        </div>
    );
}

export default UserDetail;