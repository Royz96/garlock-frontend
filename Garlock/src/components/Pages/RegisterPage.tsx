import React, { useState } from 'react';
import FormField from '../Atoms/FormField';
import { Redirect } from 'react-router-dom';
import useForm, { TFieldValidation } from '../../Util/Hooks/useForm';
import './styles.scss';
import Button from '../Atoms/Button';

export interface IRegisterFormValues {
    firstName: string,
    lastName: string,
    email: string,
    role: 'USER_CLIENT'|'SALESMAN'|'ADMIN'| ''
};


export interface IRegisterFormValidation {
    firstName: TFieldValidation,
    lastName: TFieldValidation,
    email: TFieldValidation,
    role: TFieldValidation
}

const mutation=`
mutation create($input:CreateUserInput!){
    createUser(data:$input){
      firstName
      lastName
      email
      role
    }
  }
`;


// {
//     "input":{
//       "role": "USER_CLIENT",
//       "firstName":"Luke",
//       "lastName":"Skywalker",
//       "email":"iriuer@gmail.com"
//   }
// }
    

const RegisterPage = () => {
    const [successRegister, setSuccessRegister] = useState<boolean>(false);

    const defaultFormValues: IRegisterFormValues = {
        firstName: '',
        lastName: '',
        email: '',
        role: ''
    }

    const formValidation: IRegisterFormValidation = {
        firstName: 'not-empty',
        lastName: 'not-empty',
        email: 'email',
        role: 'not-empty'
    }

    const { handleChange, values, handleBlur, errors, handleSubmit, forceUpdateValue } = useForm<IRegisterFormValues, IRegisterFormValidation>(defaultFormValues, formValidation);

    const submitForm=async()=>{
        const axios=require("axios");
        const data={firstname:values.firstName,lastname:values.lastName,email:values.email,role:values.role}
        const response= await axios.post('http://ec2-54-196-136-223.compute-1.amazonaws.com:8000/graphql',{
             query: mutation,
             variables:{data: data}
         });  
         console.log(response.data);     
     }

    return (
        <div style={{ width: '100%', height: '100%' }}>
            <div className='mainContainer'>
                <div className="register-container">
                <h2>Registro de usuarios</h2>
                    <form onSubmit={submitForm}>
                        <div className="input-container">
                        <FormField
                            type='text'
                            label='Nombre'
                            required={{ required: true, requiredMsg: 'Por favor ingresa tu nombre.' }}
                            name='firstName'
                            value={values.firstName}
                            inputElAttributes={{
                                onChange: handleChange,
                                onBlur: handleBlur
                            }}
                            error={errors.firstName}
                        />
                        <FormField
                            type='text'
                            label='Apellido'
                            required={{ required: true, requiredMsg: 'Por favor ingresa tu apellido.' }}
                            name='lastName'
                            value={values.lastName}
                            inputElAttributes={{
                                onChange: handleChange,
                                onBlur: handleBlur
                            }}
                            error={errors.lastName}
                        />
                        <FormField
                            type='text'
                            label='Correo'
                            required={{ required: true, requiredMsg: 'Por favor ingresa un correo válido.' }}
                            name='email'
                            value={values.email}
                            inputElAttributes={{
                                onChange: handleChange,
                                onBlur: handleBlur
                            }}
                            error={errors.email}
                        />
                        <FormField
                            type='text'
                            label='Rol'
                            required={{ required: true, requiredMsg: 'Por favor ingresa un Rol válido.' }}
                            name='role'
                            value={values.role}
                            inputElAttributes={{
                                onChange: handleChange,
                                onBlur: handleBlur
                            }}
                            error={errors.role}
                        />
                        </div>
                        <div className="buttonspan">
                            <Button type="primary" clickEvent={submitForm} size="default">{"Registrar"}</Button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default RegisterPage;