import React from "react";
import "./Toolbar.scss";
import DrawerToggleButton from "../SideDrawer/DrawerToggleButton";

interface ToolbarProps {
  drawerclickhandler?: any;
  name?: string;
  position?: string;
}
const Toolbar: React.FC<ToolbarProps> = props => {
  return (
    <header className="toolbar">
      <nav className="nav-toolbar">
        <div>
          <DrawerToggleButton click={props.drawerclickhandler} />
        </div>
        <div className="toolbar-logo">
          <a href="/"></a>
        </div>
        <div className="spacer"></div>
        <section>
          <label className="name">{props.name}</label>
          <label className="position">{props.position}</label>
        </section>
        <div className="toolbar-nav-items">
          <ul>
            <li>
              <a href="/login">Logout</a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
  );
};

export default Toolbar;
