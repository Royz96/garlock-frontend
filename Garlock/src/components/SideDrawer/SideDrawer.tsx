import React from 'react';
import './SideDrawer.scss';
import {faUsers,faCar,faAnchor,faFileArchive,faBuilding,faStore}from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { ComponentType } from '../Pages/Dashboard';

interface SideDrawerProps{
    show:boolean;
    onNavChange: (state: ComponentType) => void
}
const SideDrawer:React.FC<SideDrawerProps>=(props)=> {
    let drawerclasses='side-drawer';
    if(props.show){
        drawerclasses='side-drawer open';
    }
    return (
        <nav className={drawerclasses}>
            <ul>
                <li><div className="icon" onClick={() => props.onNavChange('users')}><FontAwesomeIcon icon={faUsers}/><label>Usuarios</label></div></li>
                <li><div className="icon" onClick={() => props.onNavChange('clients')}><FontAwesomeIcon icon={faStore}/><label>Clientes</label></div></li>
                <li><div className="icon" onClick={() => props.onNavChange('plants')}><FontAwesomeIcon icon={faBuilding}/><label>Plantas</label></div></li>
                <li><div className="icon" onClick={() => props.onNavChange('plants')}><FontAwesomeIcon icon={faFileArchive}/><label>Portafolio</label></div></li>
            </ul>
        </nav>
    );
}

export default SideDrawer;