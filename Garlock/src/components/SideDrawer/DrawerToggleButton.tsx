import React from 'react';
import './DrawerToggleButton.scss';

export interface TogglerProps{
    click:any;
}
const DrawerToggleButton:React.FC<TogglerProps>=(props)=> {
    return (
        <button className="toggle-button" onClick={props.click}>
            <div className="toggle-button-line"></div>
            <div className="toggle-button-line"></div>
            <div className="toggle-button-line"></div>
        </button>
    );
}

export default DrawerToggleButton;