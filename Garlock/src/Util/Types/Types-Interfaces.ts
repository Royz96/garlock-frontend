export interface IAllState {
    User: {
        name?: string,
        lastName?: string,
    },
    Auth: {
        token?: string
    }
}

export interface IResponseError {
    error: boolean,
    message: string
}