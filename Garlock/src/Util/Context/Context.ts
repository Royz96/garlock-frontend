import React, { createContext } from 'react';
import { pokemontype } from '../../components/Pages/Users';
export interface contexttype {
    user: pokemontype,
    setuser: (user: pokemontype) => any,
    logged: boolean,
    setlogged:(logged:boolean)=>any
}
export const { Provider , Consumer } = createContext<contexttype>({ user: { id: '', types: [], number: '', evolutions: [], name: '' }, setuser: () => null, logged: false, setlogged:()=>null});