import React, { Component, useState } from "react";
import Users, { pokemontype } from "./components/Pages/Users";
import { Switch, Route } from "react-router-dom";
import { ApolloProvider } from "react-apollo";
import ApolloClient from "apollo-client";
import Dashboard from "./components/Pages/Dashboard";
import Login from "./components/Pages/Login";
import RegisterPage from "./components/Pages/RegisterPage";
import { Provider } from "./Util/Context/Context";
import {HttpLink} from 'apollo-link-http';
import {InMemoryCache} from 'apollo-cache-inmemory';


const link=new HttpLink({uri:'https://graphql-pokemon.now.sh/'});
const cache= new InMemoryCache();
const client = new ApolloClient({cache,link});

const App: React.FC<{}> = () => {
  const [user, setuser] = useState<pokemontype>({
    id: "",
    name: "",
    evolutions: [],
    number: "",
    types: []
  });
  const [logged, setlogged] = useState<boolean>(false);
  return (
    <ApolloProvider client={client}>
      <Provider
        value={{
          user: user,
          setuser: setuser,
          logged: logged,
          setlogged: setlogged
        }}
      >
        <React.Fragment>
          <Switch>
            <Route exact path="/Login" component={Login} />
            <Route exact path="/" component={Dashboard} />
            <Route exact path="/users" component={Users} />
            <Route path="/registeruser" component={RegisterPage} />
            <Route path="*" component={Dashboard} />
          </Switch>
        </React.Fragment>
      </Provider>
    </ApolloProvider>
  );
};

export default App;
